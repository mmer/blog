const printPostsByDate = async (main) => { //esta función llama a la API y va pintando los últimos posts
    const posts = await fetch('http://localhost:3000/posts').then(posts => posts.json());

    const sortedPosts = []; //creo un nuevo array donde guardo las fechas sin guiones

    for (let post of posts) {
        sortedPosts.push({
            ...post
        });
    }

    for (let post of sortedPosts) {
        post.date = post.date.replace('-', '');
        for (let post of sortedPosts) {
            post.date = post.date.replace('-', '');
        }
    }

    sortedPosts.sort(function (a, b) { //aquí ordeno este nuevo array
        if (a.date < b.date) {
            return 1;
        }
        if (a.date > b.date) {
            return -1;
        }
        return 0;
    });

    div$$ = create('div', '', 'articles');

    for (let i = 0; i < 6; i++) { //aquí pintamos los 6 últimos posts

        const userId = sortedPosts[i].userId;

        const user = await fetch('http://localhost:3000/users/' + userId).then(user => user.json());

        let datePost = posts.filter(function (post) { //esto lo hacemos para buscar la fecha sin guiones en el array original
            return post.id === sortedPosts[i].id;
        });

        let article$$ = create('article', '<div class="text"><p class="sub">By ' + user.name + '</p><p class="sub">' + sortedPosts[i].category + '</p><p>' + datePost[0].date + '</p><h6>' + sortedPosts[i].title + '</h6><p>' + sortedPosts[i].sub + '</p></div>', 'post-article');
        let subdiv$$ = create('div', '<p>Continue reading...</p>', 'cta-post');
        subdiv$$.addEventListener('click', function () {
            reset();
            printPost(sortedPosts[i], user); //llamamos a esta función que pinta cada post cuando clicamos en 'Continue reading'
        })
        article$$.appendChild(subdiv$$);
        div$$.appendChild(article$$);
    }

    main.appendChild(div$$);
}

const printLastPostsPage = () => { //esta función pinta la pagina principal de los últimos posts
    printBurger();

    title$$ = create('div', '<h3>Last posts</h3>', 'title');
    app$$.appendChild(title$$);

    main$$ = create('div', ' ', 'last-posts');

    printPostsByDate(main$$);

    app$$.appendChild(main$$);

    printFooter();
}