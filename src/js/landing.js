const printLanding = () => {
    header$$ = document.createElement('header');
    buttonLogin$$ = create('button', 'Log in', 'login');
    buttonLogin$$.addEventListener('click', function () {
        reset();
        printLoginup('Log in', createFormLogin); //este botón del header llama a la función que pinta el login
    });
    header$$.appendChild(buttonLogin$$);
    app$$.appendChild(header$$);

    hero$$ = create('div', '<h3>The notepad</h3>', 'hero');

    app$$.appendChild(hero$$);

    nav$$ = document.createElement('nav');
    nav$$.classList.add('landing-nav');
    divCat$$ = create('div', '<h5>Categories</h5>', 'divCat'); //dentro del nav, este div llama a la función que pinta las categorías
    divCat$$.addEventListener('click', function () {
        reset();
        printCategories();
    });
    nav$$.appendChild(divCat$$);

    divRecent$$ = create('div', '<h5>Last posts</h5>', 'divRecent'); //este otro div llama a la función que pinta los posts más recientes
    divRecent$$.addEventListener('click', function () {
        reset();
        printLastPostsPage();
    });
    nav$$.appendChild(divRecent$$);

    form$$ = create('form', '', 'search');
    input$$ = createInput('text', 'search', 'Search', '[A-Za-z0-9 ]{4,20}', form$$);

    buttonSearch$$ = create('button', '<i class="fas fa-search"></i>', 'btn-search');
    buttonSearch$$.type = 'submit';
    buttonSearch$$.id = 'btn-search';
    form$$.appendChild(buttonSearch$$);

    divRes$$ = create('div', '', 'results');
    divRes$$.id = 'results';
    form$$.appendChild(divRes$$);

    nav$$.appendChild(form$$);

    buttonCta$$ = create('button', 'Sign up!', 'cta'); //este botón cta llama a la función que pinta el form de Signup
    buttonCta$$.addEventListener('click', function () {
        reset();
        printLoginup('Sign up!', createFormSignup);
    })
    nav$$.appendChild(buttonCta$$);

    app$$.appendChild(nav$$);

    document.getElementById('search').addEventListener('change', function () {
        search = this.value;
    });
    document.getElementById('btn-search').addEventListener('click', function (e) {
        e.preventDefault();
        searchPost(search);
    })

    printFooter();
}

printLanding(); //esta es la función que pinta la landing page