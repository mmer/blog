const searchPost = async (search) => {
    const posts = await fetch('http://localhost:3000/posts').then(posts => posts.json());

    results$$ = document.getElementById('results');
    results$$.innerHTML = '';

    let i = 0;

    for (let post of posts) {
        const user = await fetch('http://localhost:3000/users/' + post.userId).then(user => user.json());

        if (post.title.toLowerCase().includes(search.toLowerCase())) {
            i++;
            div$$ = document.createElement('div');
            p$$ = create('p', post.title, 'result-title');
            span$$ = document.createElement('span');
            span$$.innerHTML = '<br>read more!';
            span$$.addEventListener('click', function () {
                reset();
                printPost(post, user)
            })
            p$$.appendChild(span$$);
            psub$$ = create('p', post.category, 'sub')
            div$$.appendChild(p$$);
            div$$.appendChild(psub$$);
            results$$.appendChild(div$$);
        }
    }

    if (i === 0) {
        p$$ = create('p', 'No results. Try again!', 'sub')
        results$$.appendChild(p$$);
    }
}