const printPostsByAuthor = async (author) => { //esta función llama a la API y va pintando los posts correspondientes según el autor
    const authorId = author.id;
    const posts = await fetch('http://localhost:3000/posts?userId=' + authorId).then(posts => posts.json());

    div$$ = create('div', '', 'articles');

    for (let post of posts) { //aquí recorremos los posts de la categoria y pintamos el contenido y la categoria

        let article$$ = create('article', '<div class="text"><p class="sub">' + post.category + '</p><h6>' + post.title + '</h6><p>' + post.sub + '</p></div>', 'post-article');
        let subdiv$$ = create('div', '<p>Continue reading...</p>', 'cta-post');
        subdiv$$.addEventListener('click', function () {
            reset();
            printPost(post, author); //llamamos a esta función que pinta cada post cuando clicamos en 'Continue reading'
        })
        article$$.appendChild(subdiv$$);
        div$$.appendChild(article$$);
    }

    app$$.appendChild(div$$);
}

const printAuthor = (author) => { //esta función pinta una página nueva con todos los posts de un autor concreto

    printBurger();

    title$$ = create('div', '<h3>' + author.name + '</h3>', 'title');
    app$$.appendChild(title$$);

    printPostsByAuthor(author).then(printFooter);
}