const printCategory = (main, category) => { //esta función pinta cada una de las categorías
    article$$ = create('article', '<div class="overlay"><h5>' + category + '</h5></div><img src = "assets/img/' + category + '.jpg">', 'category');
    article$$.addEventListener('click', function () {
        reset();
        createCategoryPage(category);
    })
    main.appendChild(article$$);
}

const categories = ['Music', 'Travel', 'Cooking', 'Science', 'Sports', 'Culture', 'Style', 'Opinion'];

const printCategories = () => { //esta función pinta la pagina principal de las categorías disponibles
    printBurger();

    title$$ = create('div', '<h3>Categories</h3>', 'title');
    app$$.appendChild(title$$);

    main$$ = create('div', ' ', 'main-categories');

    for (let category of categories) {
        printCategory(main$$, category);
    }

    app$$.appendChild(main$$);

    printFooter();
}