const printPostsByCategory = async (category) => { //esta función llama a la API y va pintando los posts correspondientes según la categoría
    const posts = await fetch('http://localhost:3000/posts?category=' + category).then(posts => posts.json());

    div$$ = create('div', '', 'articles');

    for (let post of posts) { //aquí recorremos los posts de la categoria y pintamos el contenido y el nombre del autor
        const userId = post.userId;
        const user = await fetch('http://localhost:3000/users/' + userId).then(user => user.json()); //recogemos el objeto del autor concreto

        let article$$ = create('article', '<div class="text"><p class="sub">By ' + user.name + '</p><h6>' + post.title + '</h6><p>' + post.sub + '</p></div>', 'post-article');
        let subdiv$$ = create('div', '<p>Continue reading...</p>', 'cta-post');
        subdiv$$.addEventListener('click', function () {
            reset();
            printPost(post, user); //llamamos a esta función que pinta cada post cuando clicamos en 'Continue reading'
        })
        article$$.appendChild(subdiv$$);
        div$$.appendChild(article$$);
    }

    app$$.appendChild(div$$);
}

const createCategoryPage = (category) => { //esta función crea la pagina correspondiente para cada una de las categorías
    printBurger();

    title$$ = create('div', '<h3>' + category + '</h3>', 'title');
    app$$.appendChild(title$$);

    printPostsByCategory(category).then(printFooter);
}