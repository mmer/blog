const printBurger = () => { //esta función es la que pinta el menú desplegable burger

    const navigator = [{ //este array de objetos contiene el nombre de cada elemento del navegador desplegable y la función que llamamos al clicar 
            nav: 'Log in',
            goto: function () {
                printLoginup("Log in", createFormLogin)
            },
        },
        {
            nav: 'Sign up!',
            goto: function () {
                printLoginup("Sign up!", createFormSignup)
            },
        },
        {
            nav: 'Categories',
            goto: printCategories,
        },
        {
            nav: 'Last posts',
            goto: printLastPostsPage,
        },
        {
            nav: 'Back to main',
            goto: printLanding,
        }
    ];

    menu$$ = create('div', ' ', 'menu');
    nav$$ = document.createElement('nav');
    for (let element of navigator) { //en este for of vamos creando un a para cada elemento del navegador
        a$$ = document.createElement('a');
        a$$.href = '#';
        a$$.innerHTML = element.nav;

        a$$.addEventListener('click', function () {
            reset();
            element.goto();
        })
        nav$$.appendChild(a$$);
    }

    menu$$.appendChild(nav$$);
    app$$.appendChild(menu$$);

    toggle$$ = create('a', '<i class="fas fa-ellipsis-v"></i>', 'toggle');
    toggle$$.href = '#';
    toggle$$.addEventListener('click', function () {
        menu$$.classList.toggle('open');
        toggle$$.classList.toggle('rotate');
    })
    app$$.appendChild(toggle$$);
}