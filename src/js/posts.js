const printPost = (post, user) => { //esta función pinta un post completo con sus comentarios en una nueva página
    printBurger();

    subtitle$$ = create('h6', post.category, 'post-category');
    subtitle$$.addEventListener('click', function () {
        reset();
        createCategoryPage(post.category);
    })
    app$$.appendChild(subtitle$$);

    title$$ = create('div', '<h4>' + post.title + '</h4>', 'post-title');
    app$$.appendChild(title$$);

    sub$$ = create('div', '<h5>' + post.sub + '</h5>', 'subtitle');
    app$$.appendChild(sub$$);

    autor$$ = create('p', 'By ' + user.name + '<br>', 'sub');
    p$$ = create('p', 'More from this author', 'info');
    p$$.addEventListener('click', function () {
        reset();
        printAuthor(user);
    });
    autor$$.appendChild(p$$);
    app$$.appendChild(autor$$);

    body$$ = create('div', '<p>' + post.body + '</p>', 'post-body');
    app$$.appendChild(body$$);

    printComments(post.id).then(printFooter);
}