const checkUser = async (user$$, password$$) => { //esta función recibe el nombre de usuario y la contraseña y comprueba si existen en la base de datos de users
    const users = await fetch('http://localhost:3000/users').then(users => users.json());

    for (let user of users) {
        if (user.password === password$$ && user.username === user$$) {
            reset();
            printLanding();
            login$$ = document.querySelector('.login');
            login$$.remove();
            document.querySelector('header').innerHTML = '<p>Hello, ' + user.username + '!</p>';
            break;
        } else {
            reset();
            printLoginup('Please, try again', createFormLogin);
        }
    }
}

const createFormLogin = () => { //esta función pinta el formulario de Login
    form$$ = create('form', '', 'login-form');
    form$$.action = "javascript:alert('Hello there, I am being submitted');";
    form$$.method = 'POST';

    createLabel('user', 'User name:<br>', form$$);
    createInput('text', 'user', 'Enter your user name', '[A-Za-z0-9]{4,10}', form$$);

    createLabel('password', '<br>Password:<br>', form$$);
    createInput('password', 'password', 'Enter your password', '[A-Za-z0-9]{8,10}', form$$);

    button$$ = create('button', 'Submit', 'submit');
    button$$.type = 'submit';
    button$$.id = 'submit';

    form$$.appendChild(button$$);

    app$$.appendChild(form$$);

    document.getElementById('user').addEventListener('change', function () {
        user$$ = this.value;
    })
    document.getElementById('password').addEventListener('change', function () {
        password$$ = this.value;
    })

    document.getElementById('submit').addEventListener('click', function (e) {
        e.preventDefault();
        checkUser(user$$, password$$);
    })
}

const createFormSignup = () => { //esta función pinta el formulario de Signup
    form$$ = create('form', '', 'signup-form');
    form$$.action = "javascript:alert('Hello there, I am being submitted');";
    form$$.method = 'POST';

    createLabel('name', 'Name + last name:<br>', form$$);
    createInput('text', 'name', 'Enter your name', '[A-Za-z ]{5,40}', form$$);

    createLabel('email', '<br>Email:<br>', form$$);
    createInput('email', 'email', 'Enter your mail', '[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}', form$$);

    createLabel('user', '<br>User name:<br>', form$$);
    createInput('text', 'user', 'Enter your user name', '[A-Za-z0-9]{5,10}', form$$);

    createLabel('password', '<br>Password:<br>', form$$);
    createInput('password', 'password', 'Enter your password', '[A-Za-z0-9]{8,10}', form$$);

    button$$ = create('button', 'Submit', 'submit');
    button$$.type = 'submit';
    form$$.appendChild(button$$);

    app$$.appendChild(form$$);
}

const printLoginup = (title, form) => { //esta función pinta la página donde irá el formulario correspondiente
    div$$ = document.createElement('div'); //este primer div lleva un botón que permite volver al inicio
    div$$.classList.add('goback-div');
    buttonBack$$ = create('button', 'Go back to main', 'goback');
    buttonBack$$.addEventListener('click', function () {
        reset();
        printLanding();
    })
    div$$.appendChild(buttonBack$$)
    app$$.appendChild(div$$);

    title$$ = create('div', '<h3>' + title + '</h3>', 'title');
    app$$.appendChild(title$$);

    form(); //esta función llamará a la función que pinta el formulario de signup o de login, dependiendo de lo que queramos

    printFooter();
}