const printFooter = () => {

    const navigator = ['<i class="fab fa-instagram"></i>', '<i class="fab fa-twitter"></i>', '<i class="fab fa-facebook-square"></i>'];

    footer$$ = document.createElement('footer');
    div$$ = create('div', '<p>The notepad</p><img src="assets/img/logo.svg">', 'logo-footer');
    div$$.addEventListener('click', function () {
        reset();
        printLanding();
    })
    footer$$.appendChild(div$$);
    nav$$ = create('nav', '', 'nav-footer');

    for (nav of navigator) {
        a$$ = create('a', nav, 'a-footer');
        a$$.href = "#";
        nav$$.appendChild(a$$);
    }

    footer$$.appendChild(nav$$);

    app$$.appendChild(footer$$);
}