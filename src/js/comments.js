const printComments = async (postId) => { //esta función pinta los comentarios de un post concreto

    const comments = await fetch('http://localhost:3000/comments?postId=' + postId).then(comments => comments.json());

    divComment$$ = create('div', '<h5>Comments</h5>', 'comments-div');

    for (comment of comments) {
        div$$ = create('div', '<h6>' + capitalize(comment.name) + '</h6><p>' + capitalize(comment.sub) + '</p>', 'comment');
        divComment$$.appendChild(div$$);
    }
    app$$.appendChild(divComment$$)

    form$$ = create('form', '', 'share-feedback');
    form$$.action = "javascript:alert('Your comment has been submitted');";
    form$$.method = 'POST';
    createLabel('comment', 'Share your feedback with love', form$$);
    inputTitle$$ = create('input', '', 'comment-title');
    inputTitle$$.type = 'text';
    inputTitle$$.placeholder = 'Title';
    inputTitle$$.addEventListener('change', function () {
        commentTitle = this.value;
    })
    input$$ = create('input', '', 'comment');
    input$$.type = 'text';
    input$$.placeholder = 'Leave your comment';
    input$$.addEventListener('change', function () {
        comment = this.value;
    })
    form$$.appendChild(inputTitle$$);
    form$$.appendChild(input$$);
    button$$ = create('button', 'Send', 'submit-comments');
    button$$.addEventListener('click', function (e) {
        e.preventDefault();
        div$$ = create('div', '<h6>' + capitalize(commentTitle) + '</h6><p>' + capitalize(comment) + '</p>', 'comment');

        divComment$$.appendChild(div$$);
    })

    form$$.appendChild(button$$)

    app$$.appendChild(form$$);
}