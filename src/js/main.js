const app$$ = document.getElementById('app');

const reset = () => {
    app$$.innerHTML = ' ';
}

const create = (element, inner, clase) => {
    element$$ = document.createElement(element);
    element$$.innerHTML = inner;
    element$$.classList.add(clase);
    return element$$;
}

const createLabel = (label, inner, form) => {
    label$$ = document.createElement('label');
    label$$.for = label;
    label$$.innerHTML = inner;
    form.appendChild(label$$);
}

const createInput = (type, inputId, placeholder, pattern, form) => {
    input$$ = document.createElement('input');
    input$$.type = type;
    input$$.id = inputId;
    input$$.name = inputId;
    input$$.placeholder = placeholder;
    input$$.pattern = pattern;
    input$$.setAttribute("required", "");
    form.appendChild(input$$);
}

const capitalize = (word) => { //esta función sirve para poner la primera letra en mayúscula
    return word[0].toUpperCase() + word.slice(1);
}